/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.mnilsen.iotbutton;

import com.amazonaws.services.lambda.runtime.Context;
import com.philips.lighting.hue.listener.PHGroupListener;
import com.philips.lighting.hue.sdk.PHAccessPoint;
import com.philips.lighting.hue.sdk.PHBridgeSearchManager;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.hue.sdk.PHMessageType;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHBridgeResource;
import com.philips.lighting.model.PHGroup;
import com.philips.lighting.model.PHHueError;
import com.philips.lighting.model.PHLightState;
import com.philips.lighting.hue.sdk.PHSDKListener;
import java.util.List;
import java.util.Map;

/**
 *
 * @author michaeln
 */
public class ButtonHandler {

    private static final String HUE_DEVICE_NAME = "AWS-IotButton";
    private static final String HUE_APP_NAME = "AwsIotControl";

    private PHHueSDK phHueSDK;
    private PHBridge bridge;
    private Context context;
    private PHGroupListener grpListener;

    private String groupName;
    private String hueIpAddress;
    private String hueUsername;

    public ButtonHandler() {
        phHueSDK = PHHueSDK.create();
        Context ctx;
        this.selectBridge();
    }

    public void handleEvent(Request data, Context ctx) {
        if (this.context == null) {
            this.context = ctx;
            this.retrieveEnvironmentValues();
        }

        if (this.bridge == null) {
            this.initializeHue();
        }

        StringBuilder sb = new StringBuilder();
        String message = String.format("Received event data:\nSerial No.:\t%s\nClick Type:\t%s\nBatt. Level:\t%s",
                data.getSerialNumber(), data.getClickType(), data.getBatteryVoltage());
        sb.append(message);
        sb.append("\n");

        PHLightState lightState = new PHLightState();

        if (data.getClickType().equals("single")) {
            lightState.setBrightness(254);
            lightState.setOn(Boolean.TRUE);
        } else if (data.getClickType().equals("double")) {
            lightState.setOn(Boolean.FALSE);
        } else if (data.getClickType().equals("triple")) {
            lightState.setBrightness(128);
            lightState.setOn(Boolean.TRUE);
        }
        PHGroupListener listener = this.createGroupListener();
        bridge.setLightStateForGroup(this.groupName, lightState, listener);
        ctx.getLogger().log(sb.toString());
    }

    private void retrieveEnvironmentValues() {
        String group = context.getClientContext().getEnvironment().get("HUE_GROUP");
        if (group == null) {
            group = "Green Room";
        }
        this.groupName = group;
        String name = context.getClientContext().getEnvironment().get("HUE_USERNAME");
        if (name == null) {
            name = "sdfiodofasdofiuasodfiuoasidfu";
        }
        this.hueUsername = name;
        String ip = context.getClientContext().getEnvironment().get("HUE_IP_ADDRESS");
        if (ip == null) {
            ip = "81.21.21.21";
        }
        this.hueIpAddress = ip;
    }

    private void initializeHue() {
        phHueSDK.setAppName(HUE_APP_NAME);
        phHueSDK.setDeviceName(HUE_DEVICE_NAME);
        phHueSDK.getNotificationManager().registerSDKListener(listener);
        PHAccessPoint lastAccessPoint = new PHAccessPoint();
        lastAccessPoint.setIpAddress(hueIpAddress);
        lastAccessPoint.setUsername(hueUsername);

        if (!phHueSDK.isAccessPointConnected(lastAccessPoint)) {
            phHueSDK.connect(lastAccessPoint);
        } else {  // First time use, so perform a bridge search.
            doBridgeSearch();
        }
        this.bridge = this.phHueSDK.getSelectedBridge();
    }

    public void doBridgeSearch() {
        PHBridgeSearchManager sm = (PHBridgeSearchManager) phHueSDK.getSDKService(PHHueSDK.SEARCH_BRIDGE);
        
        sm.setPortalAddress(this.hueIpAddress);

    }

    private PHGroupListener createGroupListener() {
        if (this.grpListener != null) {
            return this.grpListener;
        }
        PHGroupListener listener = new PHGroupListener() {
            @Override
            public void onCreated(PHGroup phg) {
                //  no op
            }

            @Override
            public void onReceivingGroupDetails(PHGroup phg) {
                //  no op
            }

            @Override
            public void onReceivingAllGroups(List<PHBridgeResource> list) {
                //  no op
            }

            @Override
            public void onSuccess() {
                context.getLogger().log("Hue Group update was successful");
            }

            @Override
            public void onError(int i, String string) {
                context.getLogger().log("Hue Group update reported error: " + i + ", " + string);
            }

            @Override
            public void onStateUpdate(Map<String, String> map, List<PHHueError> list) {
                context.getLogger().log("Hue Group state updated: " + map);
            }

        };
        this.grpListener = listener;
        return listener;
    }

    private void selectBridge() {
        bridge = phHueSDK.getSelectedBridge();
    }
    
    private PHSDKListener listener = new PHSDKListener() {

        @Override
        public void onAccessPointsFound(List accessPoint) {
             // Handle your bridge search results here.  Typically if multiple results are returned you will want to display them in a list 
             // and let the user select their bridge.   If one is found you may opt to connect automatically to that bridge.            
        }
        
        @Override
        public void onCacheUpdated(List cacheNotificationsList, PHBridge bridge) {
             // Here you receive notifications that the BridgeResource Cache was updated. Use the PHMessageType to   
             // check which cache was updated, e.g.
            if (cacheNotificationsList.contains(PHMessageType.LIGHTS_CACHE_UPDATED)) {
               System.out.println("Lights Cache Updated ");
            }
        }

        @Override
        public void onBridgeConnected(PHBridge b, String username) {
            phHueSDK.setSelectedBridge(b);
            phHueSDK.enableHeartbeat(b, PHHueSDK.HB_INTERVAL);
            // Here it is recommended to set your connected bridge in your sdk object (as above) and start the heartbeat.
            // At this point you are connected to a bridge so you should pass control to your main program/activity.
            // The username is generated randomly by the bridge.
            // Also it is recommended you store the connected IP Address/ Username in your app here.  This will allow easy automatic connection on subsequent use. 
        }

        @Override
        public void onAuthenticationRequired(PHAccessPoint accessPoint) {
            phHueSDK.startPushlinkAuthentication(accessPoint);
            // Arriving here indicates that Pushlinking is required (to prove the User has physical access to the bridge).  Typically here
            // you will display a pushlink image (with a timer) indicating to to the user they need to push the button on their bridge within 30 seconds.
        }

        @Override
        public void onConnectionResumed(PHBridge bridge) {

        }

        @Override
        public void onConnectionLost(PHAccessPoint accessPoint) {
             // Here you would handle the loss of connection to your bridge.
        }
        
        @Override
        public void onError(int code, final String message) {
             // Here you can handle events such as Bridge Not Responding, Authentication Failed and Bridge Not Found
        }

        @Override
        public void onParsingErrors(List parsingErrorsList) {
            // Any JSON parsing errors are returned here.  Typically your program should never return these.      
        }
    };

}
